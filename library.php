<?php
    class library
	{
		public function output($out,$type=0)
		{
			if ($type==0)
			{
				echo $out;
				return;
			}
			elseif($type==1)
			{
				echo <<<'_END'
				<div id='success' class="w3-green w3-button">
				Success!
				</div>
				<script>
					$("$success").css("display: block;");
					setTimeout(function(){$("#success").css("display:none;");},5000);
				</script>
				$out
_END;
			}
			elseif($type==2)
			{
				echo <<<'_END'
				<div id='error'class="w3-red w3-button"></div>
				<script>
					$("#error").css("display:block;");
					$("#error").innerHTML+="<br>$out";
					setTimeout(function(){$("#error").css("display:none;");},10000);
_END;
			}
			else {
				return;
			}
		}
		public function query($query,$action="Database Query Operation")
		{
			$hn="localhost";
			$un="root";
			$pw="";
			$db = $DB_PREFIX."plugapress";
			$conn=new mysqli($hn,$un,$pw,$db);
			$c=$conn->query($query);
			if(!$c) die($this->output("Error: ".$action.", Please contact the server administrator for more info"." ".$conn->error));
			else return $c;
		}
		public function cleaner($dirty, $level=0)
		{
			if ($level>=1)
			{
				return $dirty;
			}
			else
			{
				if(get_magic_quotes_gpc()) $dirty=stripslashes($dirty);
				$dirty=strip_tags($dirty);
				return htmlentities($dirty);
			}
		}
		public function expecting($type='int',$value,$len=10)
		{
			if($type==='int')
			{
				if(is_string($value)) return $value;
				else return $value= false;
			}
			if($type==='str')
			{
				if(is_int($value) || is_float($value)) return $value;
				else return $value= false;
			}
			if(str_word_count($value)<= $len)
			{
				return $value;
			}
			else
			{
				return $value= false;
			}
		}
	}