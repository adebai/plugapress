<?php
    if(file_exists('.done')) die("already installed!");
    else{
        
    echo file_get_contents('inside.zip');
    echo"<div class='w3-white w3-center'><h1>Gathering required files...</h1></div>";
    require_once('library.php');
    require_once('setup.php');
    $l= new library();
    $sql= file_get_contents('plugapress.sql');
    echo "<p class='w3-blue'>Done.</p>";
    echo"<div class='w3-white w3-center'><h1>Preparing Database and Table contents...</h1></div>";
    echo"<div id=main>CREATING DATABASE...</div>";
    echo <<<_END
    <script>
    $.get('tam.php', function(d){let p=document.createElement('p'); p.innerText="DONE CREATING DATABASE.";p.className="w3-blue w3-center"; document.getElementById('main').appendChild(p)});
    </script>
_END;
    time_sleep_until(time()+2);
    if($l->error) die("Unable to create database! ".$l->error);
    $query="USE `$DB_PREFIX".'_plugapress`';
    $l->query($query);
    if($l->error) die("Unable to create database! An unknown error has occured");
    $install="CREATE TABLE IF NOT EXISTS`".$DB_PREFIX."_plugapress`".file_get_contents('plugapress.sql');
    $l->query($install);
    echo "<p class='w3-blue'>Done.</p>";
    echo "<p class='w3-blue'>Installation finished!</p>";
    echo "<p class='w3-blue'>Now to login details...</p>";
    time_sleep_until(time()+2);
    function get_login_details()
		{
			echo <<<'_END'
			<div class="w3-container w3-large w3-teal w3-center w3-zoom-in w3-animate" style="box-shadow:10px 10px 10px 10px rgba(0,0,0,.5), inset 3px 3px 3px 3px rgba(225,225,225,.8);position:fixed;left:50%;top:50%; z-index:9999;">
				<form class="w3-form" id="a" action="go.php" method="post">
					<p class="w3-header">Your Login Details</p>
					<div class="w3-amber">
						<p class="w3-border-left w3-teal">Username: <input name="uname" class="w3-input" type="text" placeholder="Enter Your Username Here"></p>
						<p class="w3-border-left w3-teal">Password: <input name="pword" class="w3-input" type="password" placeholder="Enter Your Password Here"></p>
						<input type="submit" class="w3-button w3-blue w3-submit" value="login">
					</div>
				</form>
            </div>
_END;
        }
    }
    get_login_details();
    $fo=fopen('.done','w');
        fwrite($fo,"done");
        fclose($fo);

    
