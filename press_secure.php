<?php
	require_once'library.php';
	$library=new library();
	class UserManager
	{
		function get_login_details()
		{
			echo <<<'_END'
			<div class="w3-container w3-medium w3-teal w3-center w3-zoom-in w3-animate" style="box-shadow:10px 10px 10px 10px rgba(0,0,0,.5), inset 3px 3px 3px 3px rgba(225,225,225,.8);position:fixed;left:50%;top:50%; z-index:9999;">
				<form class="w3-form w3-spin" action="index.php" type="post">
					<p class="w3-header">Login</p>
					<div class="w3-amber">
						<p class="w3-border-left w3-teal">Email: <input name="uname" class="w3-input" type="email" placeholder="Enter Your Email Here"></p>
						<p class="w3-border-left w3-teal">Password: <input name="pword" class="w3-input" type="password" placeholder="Enter Your Password Here"></p>
						<input type="submit" class="w3-button w3-blue w3-submit" value="login">
					</div>
				</form>
			</div>
_END;
		}
		function authenticate()
		{
			$u=$_POST['uname'];
			$p=$_POST['pword'];
			if (mb_strlen($u) >7){return intruder(1);}
			elseif(mb_strlen($u) <7){echo "Bad Details";
				header('location : '.$_SERVER['HTTP_REFERER']);}
			elseif(mb_strlen($p) >15){ return intruder(1);}
			elseif(mb_strlen($p) <15){echo "Bad Details";
				header('location : '.$_SERVER['HTTP_REFERER']);}
			else{
				$query="SELECT COUNT * FROM press_admins WHERE `_username_`=`$u` AND `_password_`=`$p`";
				$action="Selecting Users";
				$library->query($query,$action,$result);
			}
		}
		function rand2($type=0)
		{
			$constant="";
			foreach($_SERVER as $data) $constant+=$data;
			$constant+=rand()*59089748746748349;
			if($type) $constant=$_SERVER[0].$_SERVER[1].(rand()*635892397328974);
		}
		function hide_user_credentials($aarray,$rarray,$data)
		{
			$data=str_replace($aarray,$rarray,$data);
			return $data=hash('ripemd128',$data);
		}
		function encrypt_user_credentials($aarray,$rarray,$data)
		{
			$data=str_replace($aarray,$rarray,$data);
			return $data=hash('ripemd128',$data);
		}
		function start_secure_session($secure_data)
		{
			fopen('./sessions/'.$secure_data[0].'sessions','w');
			if(!fwrite($w, $secure_data))
			{
				fcloes($w);
				return false;
			} else
			{
				session_start();
				ini_set('session_use_cookie_only',"true");
				$_SESSION['data']=$secure_data[1];
				return true;
			}
		}
		function verify_hidden_credentials($data,$data_id)
		{
			if(!file_exists('./sessions/'.$data_id.'sessions'))
			{
				return false;
			}
			elseif(file_get_contents('./sessions/'.$data_id.'sessions') != $data)
			{
				return false;
			}
			elseif(file_get_contents('./sessions/'.$data_id.'sessions') == $data)
			{
				return true;
			}
			else return false;
		}
		function assign_secret_credentials()
		{
			if(!preg_match($_SERVER['PHP_SELF'],$_SERVER['HTTP_REFERER']))
			{
				return false;
			}
			else
			{
				$SESSION['secret']['ip']=$_SERVER['REMOTE_ADDR'];
				$SESSION['secret']['ua']=$_SERVER['HTTP_USER_AGENT'];
				$SESSION['secret']['page']=$_SERVER['HTTP_REFERER'];
				return true;
			}
		}
		function confirm_secret_credentials()
		{
			if ($_SESSION['secret']['ip']!=$_SERVER['REMOTE_ADDR'] || $_SESSION['secret']['ua'] != $_SERVER['HTTP_USER_AGENT'] || $_SESSION['secret']['page']!=$_SERVER['HTTP_REFERER']) { return false;}else{return true;}
		}
		function isLogged_In()
		{
			if (isset($_SESSION['data']) && isset($SESSION['secret']['ip']) && isset($SESSION['secret']['ua']) && isset($SESSION['secret']['page']) && preg_match($_SERVER['SCRIPT_HOST'],$_SERVER['HTTP_REFERER'])){ return true;}else{return false;}
		}
	}		